/*jshint esversion: 6 */

const express = require('express');
const bodyParser = require('body-parser');
const util = require(__BASEDIR + '/util');
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));

const { Status } = require(__BASEDIR + '/models');

//--- Create entry
router.post("/insert", (req, res) => {
	util.log("대시보드 상태 입력");

	var data = '';

	req.on('data', function (chunk) {
        data += chunk;
	});

	req.on('end', function () {
        console.log('POST data received');
		//console.log(data);
        jsData = JSON.parse(data)
        util.log(jsData)
		let body = {
			"rmnno":"",
			"status_value": ""
		}
		body.rmnno = jsData.rmnno;
        body.status_value = JSON.stringify(jsData.status_value)
        util.log(body.rmnno);
        util.log(jsData.status_value);
        util.log(body.status_value);
		doCreate(body);
        
        //doCreate(JSON.parse(data));
        
	});

	let doCreate = function(body) {
		//Status.create(body)
		Status.upsert(body)
		.then((data) => {
			util.log("Success to save data!");			
			res.json({
				success: true,
				msg: "",
				value: data
			})
		})
		.catch((error) => {
			console.error(error);
			res.json({
				success: false,
				msg: error,
				value: ""
			});
		});
	}

});
//----------

//--- Get Entry
router.get("/DashStatus", (req, res) => {
	util.log("대쉬보드정보 조회");
	util.log(req);
		
	let prodId = req.query.rmnno;
	util.log("rmnno:"+prodId);
	
	Status.findOne({ where: {rmnno: prodId} })
	.then((data) => {
		console.log(JSON.stringify(data));
		res.json({
			success: true,
			msg: "",
			value: data
		})
	})
	.catch((error) => {
		console.error(error);
		res.json({
			success: false,
			msg: error,
			value: ""
		});
	});

});

//--Destroy entry
router.get("/ClearDashBoard", (req, res) => {
	util.log("DashBoard 정보를 정리합니다.");

	let prodId = req.query.rmnno;

	Status.destroy({ where: {rmnno: prodId} })
	.then((data) => {
		res.json({
			success: true,
			msg: "삭제완료되었습니다.",
			value: ""
		})
	})
	.catch((error) => {
		console.error(error);
		res.json({
			success: false,
			msg: error,
			value: ""
		});
	});	
});

//--- Update entry
/*router.post("/Update", (req, res) => {
	util.log("대시보드 수정");

	var data = '';

	req.on('data', function (chunk) {
		data += chunk;
	});

	req.on('end', function () {
		console.log('POST data received');
		doUpdate(JSON.parse(data));
	});


	let doUpdate = function(body) {
		console.log(body)
		console.log(body.status_value)
		console.log(JSON.stringify(body.status_value))
		
		Status.update( " ",
		{
			where: { id: body.rmnno }
		}
		)
		.then((data) => {
			util.log("Success to update data!");			
			res.json({
				success: true,
				msg: "대시보드 정보 수정완료되었습니다.",
				value: data
			})
		})
		.catch((error) => {
			console.error(error);
			res.json({
				success: false,
				msg: error,
				value: ""
			});
		});
	}
});
*/
module.exports = router;
